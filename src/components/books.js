import React from 'react'

    const Books = ({ books }) => {
      return (
        <div>
          <center><h1>Books List</h1></center>
          {books.map((book) => (
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">{book.name}</h5>
                <h6 className="card-subtitle mb-2 text-muted">{book.id}</h6>
              </div>
            </div>
          ))}
        </div>
      )
    };

    export default Books;
