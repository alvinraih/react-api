import ReactDOM from 'react-dom'
import './index.css'
import { Route, Link, BrowserRouter as Router, Switch } from 'react-router-dom'
import App from './App'
import Home from './Home'
import React, {Component} from 'react';
import Books from './components/books';
import * as serviceWorker from './serviceWorker';


//page routing
const routing = (
  <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/books" component={App} />
        // <Route exact path="/login" component={App} />
      </Switch>
  </Router>
)
ReactDOM.render(routing, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
