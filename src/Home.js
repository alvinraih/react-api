import { Button, FormGroup, FormControl, ControlLabel, Container } from "react-bootstrap";
import "./index.css";
import React, {Component} from 'react';

class Home extends Component {

  render () {
    return (
      <Container>
        <div style={{     height:200,
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'center'}}>
          <h1>Please login to continue</h1>
        </div>
        <div style={{     height:100 ,
                          display: 'flex',
                          alignItems: 'center',
                          justifyContent: 'center'}}>
          <Button block type="submit" onClick={event =>  window.location.href='books'}>
            Login
          </Button>
        </div>
      </Container>
    );
  }
}

export default Home;
