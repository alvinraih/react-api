import axios from "axios";
import './App.css';

import React, {Component} from 'react';
import Books from './components/books';

    class App extends Component {
      state = {
        books: []
      }


      componentDidMount() {
          this.getBooks();

      }

      getBooks() {
        axios
          .get("https://jsonplaceholder.typicode.com/users")
          .then(response => {
//ganti sesuai data response
            const newBooks = response.data.map(c => {
              return {
                id: c.id,
                name: c.name
              };
            });

            const newState = Object.assign({}, this.state, {
              books: newBooks
            });

            this.setState(newState);
          })
          .catch(error => console.log(error));
      }

      render () {
        return (
          <Books books={this.state.books} />
        );
      }
}
    export default App;
